require "_settings"
do
    local sta_config = wifi.sta.getconfig(true)
    local function table_contains(table, element)
        for key, _ in pairs(table) do
            if key == element then
                return true
            end
        end
        return false
    end
    for k, v_def in pairs(_G.CUSTOM_CFG) do
        local exist, value = read_setting(k)
        if exist then
            print("Setting [\"" .. k .. "\"] <- " .. value)
            _G.CUSTOM_CFG[k] = value
        else
            local sta_config_names = { STA_SSID = "ssid", STA_PASSWORD = "pwd" }
            if table_contains(sta_config_names, k) then
                local newVal = sta_config[sta_config_names[k]]
                _G.CUSTOM_CFG[k] = newVal
                print("Setting [\"" .. k .. "\"] <- " .. newValz)
            else
                print("Setting [\"" .. k .. "\"] <- " .. v_def)
            end
        end
    end
end