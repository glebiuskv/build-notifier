require "_settings"
local baseUrl = _G.TASK_SERVER_SCHEMA .. "://" .. _G.TASK_SERVER_HOST .. ":" .. _G.TASK_SERVER_PORT
print("Base Url: "..baseUrl)
do
    local function chk_tasks()
        http.get(baseUrl .. "/tasks/list", nil, function(code, data)
            if (code < 200 or code >= 300) then
                print("HTTP request failed CODE:" .. code)
            else
                print(code, data)
                local decoder = sjson.decoder({ metatable = { __newindex = function(t, k, v)
                    print("Setting '" .. k .. "' = '" .. tostring(v) .. "'")
                    rawset(t, k, v)
                end } })
                print("-----------------------------------------\n")
                decoder:write(data)
                print("-----------------------------------------\n\n\n")
            end
        end)
    end

    local timer = tmr.create()
    timer:alarm(300000, tmr.ALARM_SINGLE, chk_tasks)
end
