local reqStr = "POST /update_settings HTTP/1.1\r\n" ..
        "Host: 192.168.4.1\r\n" ..
        "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0\r\n" ..
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n" ..
        "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3\r\n" ..
        "Accept-Encoding: gzip, deflate\r\n" ..
        "Referer: http://192.168.4.1/cfg\r\n" ..
        "Content-Type: application/x-www-form-urlencoded\r\n" ..
        "Content-Length: 124\r\n" ..
        "DNT: 1\r\n" ..
        "Connection: keep-alive\r\n" ..
        "Upgrade-Insecure-Requests: 1\r\n" ..
        "\r\n" ..
        "sta_ssid=77_4&sta_password=G52zaYQQ77&ap_ssid=ESP_BUILD_NOTIFIER&ap_password=esp123&schema=https&host=192.168.0.17&port=7070\r\n" ..
        "\r\n"

local function processSettigs(reqIn)
    local function decode(s)
        s = s:gsub('+', ' ')
        s = s:gsub('%%(%x%x)', function(h)
            return string.char(tonumber(h, 16))
        end)
        return s
    end

    local function parse_url(s)
        --s = s:match('%s+(.+)')
        local ans = {}
        for k, v in s:gmatch('([^&=]+)=([^&=]*)&?') do
            ans[k] = decode(v)
        end
        return ans
    end

    local function split(str, sep)
        local t = {};
        i = 1
        for val in string.gmatch(str, "([^" .. sep .. "]+)") do
            t[i] = val
            print("\t" .. i .. ": " .. val)
            i = i + 1
        end
        return t
    end

    print(reqIn)
    print("--------------------------")
    local param_idx = 0
    local splited = split(reqIn, "\n")
    for k, v in ipairs(splited) do
        if not (string.len(v) == 1) then
            print(string.len(v)..": '"..v.."'")
        else
            param_idx = k+1
            break
        end
    end
    print("--------------------------")
    print(param_idx)
    local result = parse_url(splited[param_idx])
    for k, v in pairs() do
        print(k..": "..v)
    end
    return result
end
processSettigs(reqStr)
