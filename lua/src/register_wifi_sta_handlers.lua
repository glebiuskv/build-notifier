require("_settings")

wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, function(T)
    print("\n\tSTA - CONNECTED" .. "\n\tSSID: " .. T.SSID .. "\n\tBSSID: " .. T.BSSID
            .. "\n\tChannel: " .. T.channel)
    local exists, setedSSID = read_setting("STA_SSID")
    --if (exists and (T.SSID == setedSSID)) then
    --    local _, setedPwd = read_setting("STA_PASSWORD")
        --wifi.sta.config({ ssid = setedSSID, pwd = setedPwd, save = true })
    --end
end)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(T)
    print("\n\tSTA - GOT_IP" .. "\n\tIP is " .. T.IP .. "\n")
end)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, function(T)
    wifi.sta.disconnect()
    local reason = T.reason
    for k, v in pairs(wifi.eventmon.reason) do
        if v == T.reason then
            reason = k
            break
        end
    end
    print("\n\tSTA - DISCONNECTED" .. "\n\tSSID: " .. T.SSID .. "\n\tBSSID: " .. T.BSSID .. "\n\treason: " .. reason)
    local exists, setedSSID = read_setting("STA_SSID")
    if (exists and not (T.SSID == setedSSID)) then
        print("Reconnecting to " .. setedSSID .. "... ")
        local _, setedPwd = read_setting("STA_PASSWORD")
        wifi.sta.config({ ssid = setedSSID, pwd = setedPwd, save = false })
        wifi.sta.connect()
    elseif exists then
        print("Connection to WiFi stoped")
    else
        print("No settings")
        print("Connection to WiFi stoped")
        wifi.setmode(wifi.SOFTAP)
    end
end)