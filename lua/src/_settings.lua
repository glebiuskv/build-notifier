_G.CUSTOM_CFG = {
    TASK_SERVER_HOST = "192.168.0.17",
    TASK_SERVER_PORT = "7070",
    TASK_SERVER_SCHEMA = "http",
    AP_SSID = "ESP_BUILD_NOTIFIER",
    AP_PASSWORD = "esp123",
    STA_SSID = "HOME",
    STA_PASSWORD = "123123",
    CHIP_NAME = node.chipid()
}

---@type string
local SETTINGS_FILE_PREFIX = "par_"
function save_setting(name, value)
    file.open(SETTINGS_FILE_PREFIX .. name, 'w') -- you don't need to do file.remove if you use the 'w' method of writing
    file.writeline(value)
    file.close()
end

function read_setting(name)
    if (file.open(SETTINGS_FILE_PREFIX .. name) ~= nil) then
        result = string.sub(file.readline(), 1, -2) -- to remove newline character
        file.close()
        return true, result
    else
        return false, nil
    end
end
