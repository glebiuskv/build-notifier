require('HttpServer')
require('_settings')

function getConfigPage()
    local sta_config = wifi.sta.getconfig(true)
    page = '<!DOCTYPE html>' ..
            '<html lang=\"en\">' ..
            '<head>' ..
            '    <meta charset=\"UTF-8\">' ..
            '    <title>CONFIG</title>' ..
            '</head>' ..
            '<body>' ..
            '<h1>CONFIGURATION</h1>' ..
            '<form method=\"post\" action=\"/update_settings\">' ..
            '    <div id=\"station\"><h2>Station</h2></div>' ..
            '    <div>' ..
            '        <div>' ..
            '            <div id=\"sta_ssid\"><h3>SSID</h3></div>' ..
            '            <input type=\"text\" name=\"STA_SSID\" aria-labelledby=\"station sta_ssid\" value=\"' .. sta_config.ssid .. '\"/>' ..
            '        </div>' ..
            '        <div>' ..
            '            <div id=\"sta_password\"><h3>Password</h3></div>' ..
            '            <input type=\"password\" name=\"STA_PASSWORD\" aria-labelledby=\"station sta_password\" value=\"' .. sta_config.pwd .. '\"/>' ..
            '        </div>' ..
            '    </div>' ..
            '    <div id=\"ap\"><h2>Accsess point</h2></div>' ..
            '    <div>' ..
            '        <div>' ..
            '            <div id=\"ap_ssid\"><h3>SSID</h3></div>' ..
            '            <input type=\"text\" name=\"AP_SSID\" aria-labelledby=\"ap ap_ssid\" value=\"' .. _G.CUSTOM_CFG.AP_SSID .. '\"/>' ..
            '        </div>' ..
            '        <div>' ..
            '            <div id=\"ap_password\"><h3>Password</h3></div>' ..
            '            <input type=\"password\" name=\"AP_PASSWORD\" aria-labelledby=\"ap ap_password\" value=\"' .. _G.CUSTOM_CFG.AP_PASSWORD .. '\"/>' ..
            '        </div>' ..
            '    </div>' ..
            '' ..
            '    <div id=\"client\"><h2>Client</h2></div>' ..
            '    <div>' ..
            '        <div>' ..
            '            <div id=\"schema\"><h3>Schema</h3></div>' ..
            '            <select type=\"\" name=\"TASK_SERVER_SCHEMA\" aria-labelledby=\"client schema\">'
    if _G.CUSTOM_CFG.TASK_SERVER_SCHEMA == "http" then
        page = page .. '' ..
                '                <option value=\"http\" selected=\"true\">http</option>' ..
                '                <option value=\"https\">https</option>'
    else
        page = page .. '' ..
                '                <option value=\"http\">http</option>' ..
                '                <option value=\"https\" selected=\"true\">https</option>'
    end
    page = page .. '' ..
            '            </select>' ..
            '        </div>' ..
            '        <div>' ..
            '            <div id=\"host\"><h3>Host</h3></div>' ..
            '            <input type=\"text\" name=\"TASK_SERVER_HOST\" aria-labelledby=\"client host\" value=\"' .. _G.CUSTOM_CFG.TASK_SERVER_HOST .. '\"/>' ..
            '        </div>' ..
            '        <div>' ..
            '            <div id=\"port\"><h3>Port</h3></div>' ..
            '            <input type=\"number\" name=\"TASK_SERVER_PORT\" aria-labelledby=\"client port\" value=\"' .. _G.CUSTOM_CFG.TASK_SERVER_PORT .. '\"/>' ..
            '        </div>' ..
            '    </div>' ..
            '' ..
            '    <div><input type=\"submit\" value=\"SAVE\"></div>' ..
            '</form>' ..
            '</body>' ..
            '</html>'
    return page
end

local function parseSettigs(reqIn)
    local function decode(s)
        s = s:gsub('+', ' ')
        s = s:gsub('%%(%x%x)', function(h)
            return string.char(tonumber(h, 16))
        end)
        return s
    end

    local function parse_url(s)
        --s = s:match('%s+(.+)')
        local ans = {}
        for k, v in s:gmatch('([^&=]+)=([^&=]*)&?') do
            ans[k] = decode(v)
        end
        return ans
    end

    local function split(str, sep)
        local t = {};
        i = 1
        for val in string.gmatch(str, "([^" .. sep .. "]+)") do
            t[i] = val
            --print("\t" .. i .. ": " .. val)
            i = i + 1
        end
        return t
    end

    --print(reqIn)
    --print("--------------------------")
    local param_idx = 0
    local splited = split(reqIn, "\n")
    for k, v in ipairs(splited) do
        if not (string.len(v) == 1) then
            --print(string.len(v)..": '"..v.."'")
        else
            param_idx = k+1
            break
        end
    end
    --print("--------------------------")
    --print(param_idx)
    local result = parse_url(splited[param_idx])
    return result
end

function processSettigs(req, res)
    local rawReq = req["raw"]
    --print(rawReq)
    local params = parseSettigs(rawReq)
    for k, v in pairs(params) do
        save_setting(k,v)
        _G.CUSTOM_CFG[k]=v
    end
    local page = '<!DOCTYPE html>' ..
            '<html lang=\"en\">' ..
            '<head>' ..
            '    <meta charset=\"UTF-8\">' ..
            '    <title>CONFIG</title>' ..
            '</head>' ..
            '<body>' ..
            '<h2>CONFIGURATION Saved</h2>' ..
            '<div><a href = "/cfg">OK</a></div>' ..
            '</body>' ..
            '</html>'
    res:send(page)
end

function decode(s)
    s = s:gsub('+', ' ')
    s = s:gsub('%%(%x%x)', function(h)
        return string.char(tonumber(h, 16))
    end)
    return s
end

function parse_url(s)
    --s = s:match('%s+(.+)')
    local ans = {}
    for k, v in s:gmatch('([^&=]+)=([^&=]*)&?') do
        ans[k] = decode(v)
    end
    return ans
end

function tableDump(table)
    for k, v in pairs(table) do
        if type(v) == 'table' then
            print("--------------->")
            tableDump(v)
        elseif type(v) == 'string' and type(k) == 'string' then
            print(k .. " :: " .. v)
        else
            if type(k) == 'string' then
                print(k .. " :: ")
            elseif type(v) == 'string' then
                print(" :: " .. v)
            else
                print(" :: ")
            end
        end
    end
end
-------------------------------------------------------------------------------------------
---------------------------------------HTTP server-----------------------------------------
-------------------------------------------------------------------------------------------

local app = express.new()
app:listen(80)

-- Register a new middleware that prints the url of every request
app:use(function(req, res, next)
    print(req.url)
    next()
end)

app:get('/cfg', function(req, res)
    local page = getConfigPage()
    res:send(page)
end)

app:post('/update_settings', function(req, res)
    --print("POST processing save....")
    --for k, v in pairs(req) do
    --    print(k)
    --end
    --print("---------------------------------")
    --tableDump(req)
    processSettigs(req, res)
end)