package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/orcaman/concurrent-map"
	"log"
	"net/http"
	"os"
	"time"
)

var taskMap = cmap.New()
var logger = log.New(os.Stdout, "http: ", log.LstdFlags)

type Task struct {
	Name    string    `json:"name"`
	Status  string    `json:"status"`
	Updated time.Time `json:"updated"`
}

type Tasks []Task

func main() {
	logger.Println("Server is starting...")

	router := mux.NewRouter().StrictSlash(true)
	router.Methods("GET").Subrouter().HandleFunc("/", Index)
	router.Methods("GET").Subrouter().HandleFunc("/tasks/list", TaskList)
	router.Methods("GET").Subrouter().HandleFunc("/tasks/get/{taskName}", TaskStatus)
	router.Methods("POST", "GET").Subrouter().HandleFunc("/tasks/upsert", TaskUpsert)

	log.Fatal(http.ListenAndServe(":7070", router))
}

func Index(w http.ResponseWriter, r *http.Request) {
	message := "Welcome!\n It is task status tracker\n Use `/tasks/*` endpoints"
	logger.Println(w, message)
	w.Write([]byte(message))
}

func TaskList(w http.ResponseWriter, _ *http.Request) {
	logger.Println(w, "Todo Index!")
	keys := taskMap.Keys()
	tasks := Tasks{}
	for i := range keys {
		v, ok := taskMap.Get(keys[i])
		if ok {
			tasks = append(tasks, v.(Task))
		}
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tasks)
}

func TaskStatus(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["taskName"]
	logger.Println(w, "Todo show:", name)
	v, ok := taskMap.Get(name)
	w.Header().Set("Content-Type", "application/json")
	if ok {
		json.NewEncoder(w).Encode(v.(Task))
	} else {
		json.NewEncoder(w).Encode(nil)
	}
}

func TaskUpsert(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	status := r.FormValue("status")
	taskMap.Set(name, Task{name, status, time.Now()})
	w.Write([]byte("OK"))
}
